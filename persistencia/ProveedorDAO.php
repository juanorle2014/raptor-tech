<?php
class ProveedorDAO
{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $admin;
    private $estado;

    public function ProveedorDAO($id="", $nombre="", $apellido="", $correo="", $clave="", $admin="", $estado=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> admin = $admin;
        $this -> estado= $estado;
        }
    
        public function Crear(){
        
            return "insert into proveedor (nombre, apellido, correo, clave, estado, administrador_idadministrador)
                    values (
                    '" . $this -> nombre . "',
                    '" . $this -> apellido . "',
                    '" . $this -> correo . "',
                    '" . md5($this -> clave). "',
                    '" . $this -> estado . "',
                    '" . $this -> admin . "'
                     )";
        }

    public function autenticar(){
        
        return "select idproveedor, estado
                    from proveedor
                    where correo = '" . $this -> correo . "' and clave = '" .md5($this -> clave). "'";
    }
    
    public function consultar(){
        
        return "select nombre, apellido, correo
                    from proveedor
                    where idproveedor = '" . $this -> id . "'";
    }

    public function ConsultarporProv($pag, $regPag){
        
        return "select idproducto, nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto, imagen
        from producto
        where proveedor_idproveedor = '" .$this ->id. "'
        limit " . (($pag - 1) * $regPag) . ", " . $regPag;
    }
    
    public function consultarNumRegProv(){
        
        return "select count(idproducto)
                from producto where proveedor_idproveedor ='" .$this -> id . "'";
    }

    public function Activar(){
        
        return "update proveedor
                    set estado = '1'
                    where idproveedor = '" . $this -> id . "'";
    }
    
    public function Deshabilitar(){
        
        return "update proveedor
                    set estado = '0'
                    where idproveedor = '" . $this -> id . "'";
    }

    public function ConsultarEstado(){
        
        return "select estado
                    from proveedor
                    where idproveedor = " . $this -> id;
    }
    
    public function ConsultarVendidosvsinv(){
        return "select p.nombre,(p.cantidad) as inventario, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto) where p.proveedor_idproveedor='". $this -> id."' 
                group by p.nombre
                order by cantidad DESC";
    }
    
    public function ConsultarVendidos(){
        return "select p.nombre, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto) where p.proveedor_idproveedor='". $this -> id."'
                group by p.nombre
                order by cantidad DESC";
    }

    
    }