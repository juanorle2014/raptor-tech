<?php

class FacturaDAO
{
    private $id;
    private $total;
    private $idcliente;
    private $estado;
    
    
    public function FacturaDAO($id="",$total="",$idc="",$estado=""){
        
        $this -> id = $id;
        $this -> total = $total;
        $this -> idcliente = $idc;
        $this -> estado = $estado;

        
    }
    
    
    public function CrearFactura(){
        
        return "insert into factura (fecha, hora, total, estado, cliente_idcliente)
          values (CURRENT_DATE,DATE_FORMAT(NOW( ), '%H:%i:%S' ), 
          '" .$this -> total ."',
          '" .$this -> estado ."',
           '". $this -> idcliente. "')";

    }
    public function Consultar(){
        return "SELECT  fecha, hora, total,nombre, apellido  from factura join  cliente on cliente.idcliente=factura.cliente_idcliente
            where factura.idfactura='" .$this -> id. "'";
    }
    public function ConsultarUltimoId(){
        
        return "select last_insert_id()";
    }
    
    public function ConsultarProductos(){
        
        return "select producto_idproducto, unidades, subtotal
                from pfactura where factura_idfactura = " . $this -> id;
    }

    public function ANoDisponible(){
        
        return "update factura
                    set estado = '1'
                    where idfactura = '" . $this -> id . "'";
    }
    
    public function ADisponible(){
        
        return "update factura
                    set estado = '0'
                    where idfactura = '" . $this -> id . "'";
    }

    public function ConsultarEstado(){
        
        return "select estado
                    from factura
                    where idfactura = " . $this -> id;
    }
    
    public function ConsultarDom(){
        return "SELECT  total,nombre, apellido  from factura join  cliente on cliente.idcliente=factura.cliente_idcliente
            where factura.idfactura='" .$this -> id. "'";
    }
}

