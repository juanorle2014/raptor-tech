<?php
class DomiciliarioDAO
{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $admin;
    private $estado;

    public function DomiciliarioDAO($id="", $nombre="", $apellido="", $correo="", $clave="", $estado=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado= $estado;
        }

        public function ConsultarUltimoId(){
            
            return "select last_insert_id()";
        }
    
        public function Crear(){
            
            return "insert into domiciliario (nombre, apellido, correo, clave, estado)
                    values (
                    '" . $this -> nombre . "',
                    '" . $this -> apellido . "',
                    '" . $this -> correo . "',
                    '" . md5($this -> clave). "',
                    '" . $this -> estado . "'
                     )";
    }

    public function Autenticar(){
            
        return "select iddomiciliario, estado
                    from domiciliario
                    where correo = '" . $this -> correo . "' and clave = '" .md5($this -> clave). "'";
    }

    public function Activar(){
        
        return "update domiciliario
                    set estado = '1'
                    where iddomiciliario = '" . $this -> id . "'";
    }
    
    public function Deshabilitar(){
        
        return "update domiciliario
                    set estado = '0'
                    where iddomiciliario = '" . $this -> id . "'";
    }

    public function ConsultarEstado(){
        
        return "select estado
                    from domiciliario
                    where iddomiciliario = " . $this -> id;
    }

    public function Consultar(){
            
        return "select nombre, apellido, correo
                    from domiciliario
                    where iddomiciliario= '" . $this -> id . "'";
    }
      
    public function BuscarDom(){
        
        return "select idfactura, total, cliente_idcliente, estado 
                    from factura
                    where estado = 0";
    }

    
    }