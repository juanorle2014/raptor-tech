<?php
class ProductoDAO{
    private $id;
    private $nombre;
    private $precio;
    private $cantidad;
    private $proveedor;
    private $marca;
    private $tipoProducto;
    private $imagen;    
    
    public function ProductoDAO($id="", $nombre="", $precio="", $cantidad="", $proveedor="", $marca="", $tipoProducto="", $imagen=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> cantidad = $cantidad;
        $this -> proveedor = $proveedor;
        $this -> marca = $marca;
        $this -> tipoProducto = $tipoProducto;
        $this -> imagen = $imagen;
    }
    
    public function crear(){
        return "insert into producto (nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto, imagen)
                values (
                '" . $this -> nombre . "',
                '" . $this -> precio . "',
                '" . $this -> cantidad . "',
                '" . $this -> proveedor . "',
                '" . $this -> marca . "',
                '" . $this -> tipoProducto . "',
                '" . $this -> imagen . "'
                )";
    }
    
    public function consultar(){
        return "select idproducto, nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto, imagen
                from producto 
                where idproducto = '" . $this -> id. "'";
    }
    
    
    public function consultarTodos(){
        return "select idproducto, nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto, imagen
                from producto"; 
    }
    
    public function consultarTodosPag($pag, $regPag){
        return "select idproducto, nombre, precio, cantidad, proveedor_idproveedor, marca_idmarca, tipoproducto_idtipoproducto, imagen
                from producto
                limit " . (($pag - 1) * $regPag) . ", " . $regPag;
    }
    
    
    
    public function consultarNumReg(){
        return "select count(idProducto) 
                from producto";
    }
    
    public function Agregar() {
        
        return "UPDATE producto SET cantidad = producto.cantidad + '" . $this -> cantidad . "'
               WHERE idproducto='" . $this -> id . "'";
    }
    public function ModPrecio() {
        
        return "UPDATE producto SET precio ='" . $this -> precio . "'
               WHERE idproducto='" . $this -> id . "'";
    }
    
    public function EditarImagen(){
        
        return "update producto set imagen = '" . $this -> imagen . "'
                where idproducto = '" . $this -> id . "'";
    }
   
    public function consultarProductosPorMarca(){
        return "select m.nombre as marca, count(p.idproducto) as cantidad
                from marca m left join producto p on (m.idmarca = p.marca_idmarca)
                group by m.nombre
                order by m.nombre asc";
    }
    
    public function consultarProductosPorTipo(){
        return "select t.nombre, count(p.idproducto) as cantidad
                from tipoproducto t left join producto p on (t.idtipoproducto = p.tipoproducto_idtipoproducto)
                group by t.nombre
                order by t.nombre asc";
    }

    public function consultarProductosPorPrecio(){
        return "(select '0 - 50k', count(idproducto)
                from producto
                where precio <= 50000)
                union
                (select '50k - 100k', count(idproducto)
                from producto
                where precio > 50000 and precio <= 100000)
                union
                (select '100k - 500k', count(idproducto)
                from producto
                where precio > 100000 and precio <= 500000)
                union
                (select '500k - 1M', count(idproducto)
                from producto
                where precio > 500000 and precio <= 1000000)
                union
                (select '1M+', count(idproducto)
                from producto
                where precio > 1000000)";
    }

    public function consultarProductosPorProveedor(){
        return "select pr.nombre, count(p.idproducto) as cantidad
                from proveedor pr left join producto p on (pr.idproveedor = p.proveedor_idproveedor)
                group by pr.nombre
                order by pr.nombre asc";
    }

    public function ConsultarVendidosvsinv(){
        return "select p.nombre,(p.cantidad) as inventario, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto  )
                group by p.nombre
                order by cantidad DESC";
    }
    
    public function ConsultarVendidos(){
        return "select p.nombre, sum(pf.unidades) as cantidad
                from pfactura pf left join producto p on (pf.producto_idproducto = p.idproducto  )
                group by p.nombre
                order by cantidad DESC";
    }

    public function consultarEnvio(){
        return "select m.nombre as domiciliario, count(p.domiciliario_iddomiciliario) as cantidad
        from domiciliario m left join envio p on (m.iddomiciliario = p.domiciliario_iddomiciliario)
        group by m.nombre
        order by m.nombre asc";
    }
    
}