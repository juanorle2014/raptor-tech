<?php
include 'presentacion/menuAdministrador.php';
$administrador = new Administrador();
$administradores = $administrador -> consultarTodos();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Administrador</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>								
								<th scope="col">Id</th>
								<th scope="col">Nombre</th>
								<th scope="col">Apellido</th>
								<th scope="col">Correo</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($administradores as $administradorActual){
							    echo "<tr>";
							    echo "<td>" . $administradorActual -> getId() . "</td>";
							    echo "<td>" . $administradorActual -> getNombre() . "</td>";
							    echo "<td>" . $administradorActual -> getApellido() . "</td>";
							    echo "<td>" . $administradorActual -> getCorreo() . "</td>";
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
					<div id="resultados"></div>
				</div>
			</div>
		</div>
	</div>
</div>

