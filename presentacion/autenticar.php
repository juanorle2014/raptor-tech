<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];

$administrador = new Administrador("", "", "", $correo, $clave);

if($administrador -> autenticar()){
    $_SESSION["rol"] = "administrador";
    $_SESSION["id"] = $administrador -> getId();     
    
    header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
}else{
    
    $proveedor= new Proveedor( "", "", "", $correo, $clave);
    
    if($proveedor ->Autenticar()){
        if($proveedor ->getEstado()==1){
            $_SESSION["rol"] = "proveedor";
            $_SESSION["id"] = $proveedor -> getId();
            header("Location: index.php?pid=" . base64_encode("presentacion/sesionProv.php"));
        }elseif ($proveedor ->getEstado()==0){
            header("Location: index.php?pid=" . base64_encode("presentacion/ingresar.php") . "&error=2");
        }
       
    }else{
        
        $cliente = new Cliente("","","",$correo,$clave);
        
        if($cliente->Autenticar()){
            if($cliente -> getEstado()==1){
                $_SESSION["rol"] = "cliente";
                $_SESSION["id"] = $cliente -> getId();
                header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
                
            }elseif ($cliente -> getEstado()==0){
                header("Location: index.php?pid=" . base64_encode("presentacion/ingresar.php") . "&error=2");
            }
            
        }else{
            $dom = new Domiciliario("","","",$correo,$clave);

            if($dom->Autenticar()){
                if($dom -> getEstado()==1){
                    $_SESSION["rol"] = "domiciliario";
                    $_SESSION["id"] = $dom -> getId();
                    header("Location: index.php?pid=" . base64_encode("presentacion/sesionDom.php"));
                    
                }elseif ($dom -> getEstado()==0){
                    header("Location: index.php?pid=" . base64_encode("presentacion/ingresar.php") . "&error=2");
                }
            }else{
            header("Location: index.php?pid=" . base64_encode("presentacion/ingresar.php") . "&error=1");

            }
        }
        
    }
    
}
?>