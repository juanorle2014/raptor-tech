<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container">
	<div class="row">
		<div class="col">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container-fluid">
					<a class="navbar-brand"
						href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
						class="fas fa-home"></i></a>
					<button class="navbar-toggler" type="button"
						data-bs-toggle="collapse" data-bs-target="#navbarNav"
						aria-controls="navbarNav" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
						<ul class="navbar-nav me-auto mb-2 mb-lg-0">
							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-expanded="false">Productos</a>
								<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
									<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php") ?>">Consultar</a></li>
									<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoPaginacion.php") ?>">Consultar Paginacion</a></li>
								</ul></li>

								<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
									href="#" id="navbarDropdownMenuLink" role="button"
										data-bs-toggle="dropdown" aria-expanded="false"> Cliente </a>
										<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
										<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/consultarCliente.php")?>">Consultar</a></li>
								</ul></li>

								<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
									href="#" id="navbarDropdownMenuLink" role="button"
									data-bs-toggle="dropdown" aria-expanded="false"> Proveedor </a>
										<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
											<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/consultarProv.php")?>">Consultar</a></li>
											<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/proveedor/crearProveedor.php")?>">Crear</a></li>
								</ul></li>

								<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
									href="#" id="navbarDropdownMenuLink" role="button"
									data-bs-toggle="dropdown" aria-expanded="false"> Domiciliario </a>
										<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
											<li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/domiciliario/consultarDom.php")?>">Consultar</a></li>
											
								</ul></li>
						</ul>

						<ul class="navbar-nav">
							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="#">Editar Perfil</a> <a
										class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/administrador/consultarAdmin.php") ?>">Consultar Administradores</a>
								</div></li>
							<li class="nav-item"><a class="nav-link"
								href="index.php?sesion=false">Cerrar Sesion</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>
