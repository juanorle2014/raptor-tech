<?php

$prov = new Proveedor($_SESSION["id"]);

$productosvvsinv= $prov -> ConsultarVendidosvsinv();
$productosvendidos = $prov -> ConsultarVendidos();
?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas Proveedor</h5>
				<div class="card-body ">
				<div id="productosvendidos" style="width: 1000px; height: 300px;"></div>
				<div id="productosvs" style="width: 1000px; height: 300px;"></div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart','bar']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

    var dataVs = google.visualization.arrayToDataTable([
        ['Nombre', 'Inventario', 'Vendidos'],
        <?php 
                foreach ($productosvvsinv as $p)
                    echo "['" . $p[0] . "', " . $p[1] . ", " . $p[2] ."],\n";        
               
               ?>
      ]);

      var optionsVs = {
        chart: {
        	 title: "Productos vendidos y en inventario",
             legend: { position: "center" },
          
        }
      };

      var chartVs = new google.charts.Bar(document.getElementById("productosvs"));

      chartVs.draw(dataVs,(optionsVs));

      var data = google.visualization.arrayToDataTable([
    	  ['Nombre', 'unidades vendidas'],
          <?php 
                  foreach ($productosvendidos as $p)
                      echo "['" . $p[0] . "', " . $p[1] ."],\n";        
                 
                 ?>
        ]);

        var options = {
          title: 'VENTAS PRODUCTOS',
          pieSliceText: 'label',
          pieHole: 0.4,
          
        };

        var chart = new google.visualization.PieChart(document.getElementById('productosvendidos'));
        chart.draw(data, options);
}
</script>