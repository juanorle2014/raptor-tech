<?php
require_once "logica/Proveedor.php";

$proveedor = new Proveedor($_GET["id"]);
$proveedor -> ConsultarEstado();

if($proveedor -> getEstado() == 1){
    
    $proveedor -> Deshabilitar();
    echo "Deshabilitado";
    
}else if($proveedor -> getEstado() == 0){
    
    $proveedor -> Activar();
    echo "Activo";
}