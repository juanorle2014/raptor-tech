<?php

$rol = $_SESSION["rol"];

if( $rol=="administrador"){
    include 'presentacion/menuAdministrador.php';
}elseif ($rol=="cliente"){
    include 'presentacion/menuCliente.php';
}

$regPag = 5;
$pag = 1;
if(isset($_GET["pag"])){
    $pag = $_GET["pag"];
}
if(isset($_GET["regPag"])){
    $regPag = $_GET["regPag"];
}

$producto = new Producto();
$productos = $producto -> consultarTodosPag($pag, $regPag);
$numReg = $producto -> consultarNumReg();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Productos Paginacion</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>N�</th>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Cantidad</th>
								<th>Marca</th>
								<th>Tipo</th>
								<th>Proveedor</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
								<?php
                            $pos = 1;
                            foreach ($productos as $productoActual) {
                                echo "<tr>";
                                echo "<td>" . $pos ++ . "</td>
                                      <td>" . $productoActual -> getNombre() . "</td>
                                      <td>" . $productoActual -> getPrecio() . "</td>
                                      <td>" . $productoActual -> getCantidad() . "</td>
                                      <td>" . $productoActual -> getMarca() -> getNombre() . "</td>
                                      <td>" . $productoActual -> getTipoProducto() -> getNombre() . "</td>
                                      <td>" . $productoActual -> getProveedor() -> getNombre() . " " . $productoActual -> getProveedor() -> getApellido() . "</td>";
                                       echo "</tr>";
                            }
                            ?>
						</tbody>
					</table>
					<div class="text-center">
						<nav aria-label="Page navigation example">
							<ul class="pagination">
								<li class="page-item <?php echo ($pag == 1)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPaginacion.php") . "&pag=" . ($pag-1) ?>"
									aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
								</a></li>
                <?php
                $botones = $numReg / $regPag;
                if($numReg%$regPag != 0){
                  $botones++;
                }
                for($i=1; $i<=$botones; $i++){
                  echo "<li class='page-item " . (($i==$pag)?"active":"") . "'>";
                  if($pag==$i){
                    echo "<span class='page-link'>" . $i . "</span>";
                  }else{
                    echo "<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPaginacion.php") . "&pag=" . $i . "'>" . $i . "</a>";
                  }
                  echo "</li>";
                }
                ?>
								<li class="page-item <?php echo ($pag == $botones)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPaginacion.php") . "&pag=" . ($pag+1) ?>"
									aria-label="Next"> <span aria-hidden="true">&raquo;</span>
								</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
