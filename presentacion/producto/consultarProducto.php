<?php
include 'presentacion/menuAdministrador.php';

$producto= new Producto();
$productos= $producto ->consultarTodos();

//echo $productos[0]->getProveedor()->getNombre();
?>
<div class="container">

	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Productos</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Cantidad</th>
								<th>Marca</th>
								<th>Tipo</th>
								<th>Proveedor</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
								<?php
                            $pos = 1;
                            foreach ($productos as $productoActual) {
                                echo "<tr>";
                                echo "<td>" . $pos ++ . "</td>
                                      <td>" . $productoActual -> getNombre() . "</td>
                                      <td>" . $productoActual -> getPrecio() . "</td>
                                      <td>" . $productoActual -> getCantidad() . "</td>
                                      <td>" . $productoActual -> getMarca() -> getNombre() . "</td>
                                      <td>" . $productoActual -> getTipoProducto() -> getNombre() . "</td>
                                      <td>" . $productoActual -> getProveedor() -> getNombre() . " " . $productoActual -> getProveedor() -> getApellido() . "</td>";
                                       echo "</tr>";
                            }
                            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>