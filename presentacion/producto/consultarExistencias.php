
<div class="container">
	<div class="row mt-2">
		<div class="col-12 ">
			<div class="card" align="center">
				<h5 class="card-header">Productos</h5>
				<div class="card-body">
				<?php

$regPag = 12;
$pag = 1;
if(isset($_GET["pag"])){
    $pag = $_GET["pag"];
}
if(isset($_GET["regPag"])){
    $regPag = $_GET["regPag"];
}

$producto = new Producto();
$productos = $producto -> consultarTodosPag($pag, $regPag);
$numReg = $producto -> consultarNumReg();
?>

<div class="row"> 
<?php
                  
    foreach ($productos as $productoActual) {

?>
      
      <div class="col-3"> 
          <div class="card">
				<form action="index.php?pid=<?php echo base64_encode("presentacion/sesionCliente.php")."&id=".$productoActual->getId()."&pag=" . ($pag) ?>" method="post">
              <img class="card-img-top" src="<?php echo $productoActual -> getImagen() ?>" >
                  <div class="card-body">
                      <h5 class="card-title"><?php echo $productoActual -> getNombre() ?> </h5>
					  <h6>$<?php echo number_format($productoActual -> getPrecio(),0) ?> COP</h6>
					  <h6>Existencias : <?php echo $productoActual -> getCantidad() ?></h6>
					  <?php $cantidad=$productoActual -> getCantidad(); ?>
					  <?php if($cantidad>=5){?>
					<select class="form-select" name="cantidad">
           				<option selected>elegir cantidad</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
				</select>
				<?php } else if($cantidad >0 && $cantidad < 5){ ?>
		   		<select class="form-select" name="cantidad">
				   <option selected>elegir cantidad</option>
				<?php  for($i = 1 ; $i<=$cantidad;$i++) { 
		        echo "<option value='" . $i . "'>" . $i. "</option>";
		   					 }?>
		    
		    </select>
		<?php } else if ($cantidad == 0){?>
		<select class="form-select" name="cantidad">
		    <option value="0"> Fuera de Stock </option>
		    </select>
		<?php }?>
					
					<br>
					  <button type="submit" name="agregar" class="btn btn-primary">Agregar al Carrito</button>
                  </div>
	</form>
            </div> 
      </div>


	

<?php } ?>

</div>

<div class="text-center">
						<nav aria-label="Page navigation example">
							<ul class="pagination">
								<li class="page-item <?php echo ($pag == 1)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/sesionCliente.php") . "&pag=" . ($pag-1) ?>"
									aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
								</a></li>
                <?php
                $botones = $numReg / $regPag;
                if($numReg%$regPag != 0){
                  $botones++;
                }
                for($i=1; $i<=$botones; $i++){
                  echo "<li class='page-item " . (($i==$pag)?"active":"") . "'>";
                  if($pag==$i){
                    echo "<span class='page-link'>" . $i . "</span>";
                  }else{
                    echo "<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/sesionCliente.php") . "&pag=" . $i . "'>" . $i . "</a>";
                  }
                  echo "</li>";
                }
                ?>
								<li class="page-item <?php echo ($pag == $botones)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/sesionCliente.php") . "&pag=" . ($pag+1) ?>"
									aria-label="Next"> <span aria-hidden="true">&raquo;</span>
								</a></li>
							</ul>
						</nav>
					</div>
					
				</div>

				

			</div>

		</div>

	</div>

</div>