<?php 
include 'presentacion/menuProv.php';


$regPag = 5;
$pag = 1;
if(isset($_GET["pag"])){
    $pag = $_GET["pag"];
}
if(isset($_GET["regPag"])){
    $regPag = $_GET["regPag"];
}

$prov=new Proveedor($_SESSION["id"]);
$productos = $prov->ConsultarporProv($pag, $regPag);
$numReg = $prov->ConsultarTotalFilasporProv();
$p = count($productos);


for($i=0;$i<$p;$i++){

    if(isset($_POST["bot".$i])){
       
        $idp=$_GET["id"];
      
        $prod=new Producto($idp,"","",$_POST["cant".$i],"","","","");
        $prod -> Agregar();
        
        $productos = $prov->ConsultarporProv($pag, $regPag);
    }
    if(isset($_POST["botp".$i])){
        $idp=$_GET["id"];
        
        $prod = new Producto($idp,"",$_POST["precio".$i],"","","","","");
        $prod ->ModPrecio();
        
        $productos = $prov->ConsultarporProv($pag, $regPag);
        
    }
}


?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Productos Poveedor</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Precio</th>
								<th>Precio</th>
								<th>Modificar</th>
								<th>Cantidad</th>
								<th>imagen</th>
								<th>Marca</th>
								<th>Tipo</th>
								<th><i class="fas fa-edit"></i></th>
							</tr>
						</thead>
						<tbody>
						<?php 
								$pos=0;
								foreach ($productos as $productoActual) { $pos ++;  ?>
								   
								    <tr>
											
								      <td class="text-center" width="20%"><?php echo $productoActual ->getNombre() ?></td>
                                      <td class="text-center" width="15%"><?php echo  $productoActual -> getPrecio() ?></td>
                                      <td class="text-center" width="10%">
                                       <form action="index.php?pid=<?php echo  base64_encode("presentacion/producto/consultarproductosprov.php"). "&id=" . $productoActual -> getId()?>" method="post">
                    						<div class=row-1>
                    							
                    							<input width="10%" type="number" name=<?php echo "'precio".($pos-1)."'" ?>>
                    								</div>
													
                    								
                    								<div class=row-1>
                    								<button class="btn btn-success" type="submit" 
                    									name=<?php echo "'botp" .($pos-1). "'" ?>>Cambiar</button>
                    									</div>
                    					</form>
                    					 </td>
                                      <td class="text-center" width="10%">
                    					<form action="index.php?pid=<?php echo  base64_encode("presentacion/producto/consultarproductosprov.php"). "&id=" . $productoActual -> getId()?>" method="post">
                    						<div class=row>
                    							
                    								<select class="form-select text-center" 
                    									name=<?php echo "'cant" .($pos-1). "'" ?>>
                    
                    									<option value="5" >5</option>
                    									<option value="10" >10</option>
                    									<option value="15" >15</option>
                    									<option value="20" >20</option>
                    									<option value="25" >25</option>
                    
                    								</select>
                    								</div>
                    								
                    								<div class=row>
                    								<button class="btn btn-success" type="submit" 
                    									name=<?php echo "'bot" .($pos-1). "'" ?>>Agregar</button>
                    									</div>
                    					</form>
                    				</td>
                                   	  		 <td class="text-center" width="5%"> <?php echo $productoActual -> getCantidad()  ?></td>
                                		 			<?php echo "<td width='15%' class='text-center'>".(($productoActual -> getImagen()!="")?"<img src='" . $productoActual -> getImagen() . "' height='40px' />":"") . "</td>" ?>
                              		       <td class="text-center"  width="10%"> <?php echo $productoActual -> getMarca() -> getNombre()  ?></td>
                                   		   <td class="text-center" width="10%"> <?php echo $productoActual -> getTipoproducto() -> getNombre()  ?></td>
                                     	<?php echo "<td nowrap width='5%' > <a href='index.php?pid=" . base64_encode("presentacion/producto/editarImagen.php") . "&id=" . $productoActual -> getId() ."' data-bs-toggle='tooltip' data-bs-placement='top' title='Editar imagen'><i class='fas fa-file-image'></i></a></td>";?>
								   </tr>
						
					<?php } ?>
						</tbody>
					</table>
					<div class="text-center">
						<nav aria-label="Page navigation example">
							<ul class="pagination">
								<li class="page-item <?php echo ($pag == 1)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarproductosprov.php") . "&pag=" . ($pag-1) ?>"
									aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
								</a></li>
                <?php
                $botones = $numReg / $regPag;
                if($numReg%$regPag != 0){
                  $botones++;
                }
                for($i=1; $i<=$botones; $i++){
                  echo "<li class='page-item " . (($i==$pag)?"active":"") . "'>";
                  if($pag==$i){
                    echo "<span class='page-link'>" . $i . "</span>";
                  }else{
                    echo "<a class='page-link' href='index.php?pid=" . base64_encode("presentacion/producto/consultarproductosprov.php") . "&pag=" . $i . "'>" . $i . "</a>";
                  }
                  echo "</li>";
                }
                ?>
								<li class="page-item <?php echo ($pag == $botones)?"disabled":"" ?> "><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarproductosprov.php") . "&pag=" . ($pag+1) ?>"
									aria-label="Next"> <span aria-hidden="true">&raquo;</span>
								</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>