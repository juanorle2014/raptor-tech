<?php
require_once "logica/Domiciliario.php";

$proveedor = new Domiciliario($_GET["id"]);
$proveedor -> ConsultarEstado();

if($proveedor -> getEstado() == 1){
    
    $proveedor -> Deshabilitar();
    echo "Deshabilitado";
    
}else if($proveedor -> getEstado() == 0){
    
    $proveedor -> Activar();
    echo "Activo";
}