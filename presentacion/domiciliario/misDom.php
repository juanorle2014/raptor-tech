<?php
include "presentacion/menuDom.php";

$admin = new Envio("","",$_SESSION["id"]);
$Doms = $admin -> Buscar();


?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Mis Domicilios</h5>
				<div class="card-body">					
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>ID Domicilio</th>
								<th>ID Factura</th>
								<th>Total</th>
								<th>Cliente</th>
								<th>Domiciliario</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
                            $pos = 1;
                            foreach ($Doms as $DomActual) {
                                echo "<tr>";
                                echo "<td>" . $DomActual -> getId() . "</td>
                                      <td>" . $DomActual -> getFactura() . "</td>
									  <td><p> $" . number_format($DomActual -> getCliente()[0],0 )." COP<p></td>
									  <td>" . $DomActual -> getCliente()[1] ." ".$DomActual -> getCliente()[2]. "</td>
                                      <td>" . $DomActual -> getDomiciliario() -> getNombre() ." ".$DomActual -> getDomiciliario() -> getApellido(). "</td>";
                                      echo "</tr>";
                            }
                            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
