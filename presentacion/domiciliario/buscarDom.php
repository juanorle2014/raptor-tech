<?php
include "presentacion/menuDom.php";
$admin = new Domiciliario();
$Doms = $admin -> BuscarDom();


?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Domicilios Disponibles</h5>
				<div class="card-body">					
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Total</th>
								<th>Cliente</th>
								<th>Estado</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
                            $pos = 1;
                            foreach ($Doms as $DomActual) {
                                echo "<tr>";
                                echo "<td>" . $DomActual -> getId() . "</td>
                                      <td>" . $DomActual -> getTotal() . "</td>
                                      <td>" . $DomActual -> getIdcliente() . "</td>
                                      <td>" . ((($DomActual -> getEstado()==1)?"<div id='capaEstado" . $DomActual -> getId() . "'>En Proceso</div>":"<div id='capaEstado" . $DomActual -> getId() . "'>Disponible</div>")) . "</td>
                                      <td nowrap>" . (($DomActual -> getEstado()==0)?"<a href='#'><i id='capaIcono" . $DomActual -> getId() . "' class='fas fa-user-times'></i></a>":(($DomActual -> getEstado()==1)?"<a href='#'><i id='capaIcono" . $DomActual -> getId() . "' class='fas fa-user-check'></i></a>":"")) . " </td>";
                                echo "</tr>";
                            }
                            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
<?php 

foreach ($Doms as $DomActual) {
    
        echo "$('#capaIcono" . $DomActual -> getId() . "').click(function() {\n";
        echo "\tvar url = 'indexAjax.php?pid=" . base64_encode("presentacion/domiciliario/cambiarEstadofacAjax.php") . "&id=" . $DomActual -> getId() . "';\n";
        echo "\t$('#capaEstado" . $DomActual -> getId() . "').load(url);\n";
        echo "\tif($('#capaIcono" . $DomActual -> getId() . "').attr('class') == 'fas fa-user-times'){\n";
        echo "\t\t$('#capaIcono" . $DomActual -> getId() . "').attr('class', 'fas fa-user-check');\n";
        echo "\t}else{\n";
        echo "\t\t$('#capaIcono" . $DomActual -> getId() . "').attr('class', 'fas fa-user-times');\n";
        echo "\t}\n";
        echo "});\n";        
    
}
?>
</script>