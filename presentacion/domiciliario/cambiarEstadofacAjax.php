<?php
require_once "logica/Factura.php";

$id=$_GET["id"];
$prov=$_SESSION["id"];

$proveedor = new Factura($_GET["id"]);
$proveedor -> ConsultarEstado();

if($proveedor -> getEstado() == 1){
    
    $proveedor -> ADisponible();
    echo "Disponible";
    
}else if($proveedor -> getEstado() == 0){
    
    $proveedor -> ANoDisponible();
    echo "NoDisponible";
    $envio = new Envio("0",$id,$prov);
    $envio -> Crear();
}