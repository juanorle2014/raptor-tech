<?php
require_once 'fpdf/fpdf.php';
require_once 'logica/Producto.php';
require_once 'logica/Proveedor.php';
require_once 'logica/Factura.php';
require_once 'logica/TipoProducto.php';
require_once 'logica/Marca.php';

if(isset($_POST['putito'])){
    
$fac = new Factura($_POST['fac']);
$productos = $fac->ConsultarProductos();
$res=$fac->Consultar();
}
$pdf = new FPDF("P","mm","LETTER");
$pdf -> AddPage();

$pdf->SetFont("Times","BI",18);
$pdf -> Image("img/logo.jpeg", 10, 10, 30, 30);
$pdf->Cell(196,20,"Raptor-Tech",0,1,"C");
$pdf->SetFont("Helvetica","BI",18);
$pdf->Cell(196,10,"Factura N. ". " ".$_POST['fac'],0,1,"C");

$pdf->Cell(196,5," ",0,1,"L");
$pdf->SetFont("Times","B",13);
$pdf->Cell(196,10,"FECHA: ".$res[0],0,1,"R");
$pdf->Cell(196,10,"HORA: ".$res[1],0,1,"R");
$pdf->Cell(196,10,"CLIENTE: ".$res[3]." ".$res[4],0,1,"R");
$pdf->Cell(196,10," ",0,1,"L");

$pdf->SetFont("Times","B",12);
$pdf->Cell(83,8,"Descripcion",1,0,"C");
$pdf->Cell(20,8,"Cantidad",1,0,"C");
$pdf->Cell(49,8,"Precio",1,0,"C");
$pdf->Cell(49,8,"Subtotal",1,1,"C");
$pdf->Cell(196,5," ",0,1,"L");


$pdf->SetFont("Times","",12);
foreach ($productos as $productoActual) {
    $alto = 8;
    if(strlen($productoActual -> getNombre()) <= 33){
        $pdf->Cell(83,$alto, $productoActual -> getNombre(),1,0,"L");
    }else {
        $pdf->Cell(83,$alto, substr($productoActual -> getNombre(), 0, 40) . "...",1,0,"L");
    }
    $pdf->Cell(20,$alto, $productoActual -> getCantidad(),1,0,"C");
    $pdf->Cell(49,$alto, "$ " . $productoActual -> getPrecio(),1,0,"C");
    $pdf->Cell(49,$alto, "$ " . $productoActual -> getPrecio() * $productoActual->getCantidad() ,1,1,"C");
}
$pdf->Cell(196,10," ",0,1,"L");
$pdf->SetFont("Times","BI",18);
$pdf->Cell(196,10,"TOTAL: $ ".number_format($res[2],2),0,1,"R");
$pdf -> Output();
$pdf -> Close();
?>