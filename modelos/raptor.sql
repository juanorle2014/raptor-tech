-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-03-2022 a las 19:15:40
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `raptor`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idadministrador`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(1, 'Juan', 'Ortiz', 'jo@123.com', '674f33841e2309ffdd24c85dc3b999de');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `idcarrito` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `cliente_idcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`idcarrito`, `estado`, `cliente_idcliente`) VALUES
(15, 0, 7),
(16, 1, 8),
(17, 0, 7),
(18, 0, 7),
(19, 0, 7),
(20, 0, 7),
(21, 0, 9),
(22, 0, 9),
(24, 0, 7),
(25, 1, 7),
(26, 1, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombre`, `apellido`, `correo`, `clave`, `direccion`, `estado`) VALUES
(7, 'Maria', 'Letrado', 'ml@123.com', '9830e1f81f623b33106acc186b93374e', 'in one house', 1),
(8, 'Homero', 'Simpson', 'hs@123.com', '789406d01073ca1782d86293dcfc0764', 'In Springfield', 1),
(9, 'Juan', 'Letrado', 'jl@123.com', '54768df6eeb13e17b9524dbd9bb44a24', 'Dig 67 b sur n 28-21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domiciliario`
--

CREATE TABLE `domiciliario` (
  `iddomiciliario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `domiciliario`
--

INSERT INTO `domiciliario` (`iddomiciliario`, `nombre`, `apellido`, `correo`, `clave`, `estado`) VALUES
(1, 'Franklin', 'Lara', 'fl@123.com', '3d296788f2a7e19ffbd912521d94a5f4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envio`
--

CREATE TABLE `envio` (
  `idenvio` int(11) NOT NULL,
  `factura_idfactura` int(11) NOT NULL,
  `domiciliario_iddomiciliario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `envio`
--

INSERT INTO `envio` (`idenvio`, `factura_idfactura`, `domiciliario_iddomiciliario`) VALUES
(2, 13, 1),
(3, 14, 1),
(4, 11, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idfactura` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `total` int(10) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0,
  `cliente_idcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idfactura`, `fecha`, `hora`, `total`, `estado`, `cliente_idcliente`) VALUES
(11, '2022-03-28', '11:47:05', 289900, 1, 7),
(12, '2022-03-29', '17:45:41', 216950, 0, 7),
(13, '2022-03-29', '18:22:37', 13586456, 1, 7),
(14, '2022-03-29', '19:28:37', 1159600, 1, 7),
(15, '2022-03-29', '19:46:41', 12679800, 0, 9),
(16, '2022-03-29', '23:20:54', 8637000, 0, 7),
(17, '2022-03-30', '11:59:18', 604200, 0, 7),
(18, '2022-03-30', '12:01:31', 1700000, 0, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`) VALUES
(1, 'Motorola'),
(2, 'Samsung'),
(3, 'Apple'),
(4, 'Xiaomi'),
(5, 'Huawei'),
(6, 'Lenovo'),
(7, 'Tencent'),
(8, 'Sony'),
(9, 'Microsoft '),
(10, 'Activision'),
(11, 'EA'),
(12, 'Ubisoft'),
(13, 'Play Station'),
(14, 'Nintendo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pcarrito`
--

CREATE TABLE `pcarrito` (
  `carrito_idcarrito` int(11) NOT NULL,
  `producto_idproducto` int(11) NOT NULL,
  `cantidad` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pcarrito`
--

INSERT INTO `pcarrito` (`carrito_idcarrito`, `producto_idproducto`, `cantidad`) VALUES
(15, 62, 1),
(17, 64, 1),
(18, 1, 2),
(18, 3, 3),
(18, 63, 3),
(18, 66, 3),
(19, 62, 4),
(20, 6, 3),
(21, 2, 2),
(21, 69, 3),
(21, 71, 3),
(22, 67, 1),
(24, 65, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pfactura`
--

CREATE TABLE `pfactura` (
  `factura_idfactura` int(11) NOT NULL,
  `producto_idproducto` int(11) NOT NULL,
  `unidades` int(1) NOT NULL,
  `subtotal` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pfactura`
--

INSERT INTO `pfactura` (`factura_idfactura`, `producto_idproducto`, `unidades`, `subtotal`) VALUES
(11, 62, 1, 289900),
(12, 64, 1, 216950),
(13, 1, 2, 9999980),
(13, 3, 3, 2700000),
(13, 63, 3, 193500),
(13, 66, 3, 692976),
(14, 62, 4, 1159600),
(15, 2, 2, 4680000),
(15, 69, 3, 2873700),
(15, 71, 3, 5126100),
(16, 6, 3, 8637000),
(17, 65, 3, 604200),
(18, 67, 1, 1700000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `precio` int(10) NOT NULL,
  `cantidad` varchar(45) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `proveedor_idproveedor` int(11) NOT NULL,
  `marca_idmarca` int(11) NOT NULL,
  `tipoproducto_idtipoproducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `nombre`, `precio`, `cantidad`, `imagen`, `proveedor_idproveedor`, `marca_idmarca`, `tipoproducto_idtipoproducto`) VALUES
(1, 'Xbox Serie X Halo Infinite Edición Limitada', 4999990, '15', 'imagenes/20220328030422.png', 7, 9, 4),
(2, ' iPhone X  64GB ', 2340000, '15', 'imagenes/20220328024754.jpeg', 5, 3, 1),
(3, 'Motorola One Fusion +', 900000, '17', 'imagenes/20220328023513.jpeg', 5, 1, 1),
(4, 'Motorola G20', 714900, '17', 'imagenes/20220328023914.jpeg', 5, 1, 1),
(5, 'Motorola G60', 1189000, '7', 'imagenes/20220328023955.jpeg', 5, 1, 1),
(6, 'iPhone 11 (128 GB) - Blanco', 2879000, '7', 'imagenes/20220328024555.jpeg', 5, 3, 1),
(61, 'Halo: The Master Chief Collection ', 330000, '17', 'imagenes/20220328030114.jpeg', 7, 9, 3),
(62, 'Halo Infinite Edición Steelbook', 289900, '1', 'imagenes/20220328031155.jpeg', 7, 9, 3),
(63, 'Horizon Zero Dawn Complete Edition ', 64500, '7', 'imagenes/20220328031834.jpeg', 7, 8, 3),
(64, 'Horizon forbidden west', 216950, '7', 'imagenes/20220328031943.jpeg', 7, 8, 3),
(65, 'Call of Duty: Vanguard Standard', 201400, '17', 'imagenes/20220328032319.jpeg', 7, 10, 3),
(66, 'Call of Duty: Black Ops Cold War', 230992, '12', 'imagenes/20220328032437.jpeg', 7, 10, 3),
(67, 'Consola Ps4 1tb + 1 Control + 3 Juegos', 1700000, '3', 'imagenes/20220328033029.jpeg', 7, 13, 4),
(68, 'Nintendo Switch OLED 64GB', 1899900, '7', 'imagenes/20220328033227.jpeg', 7, 14, 4),
(69, 'Tablet Lenovo Tab M10 FHD Plus  128GB ', 957900, '17', 'imagenes/20220328033703.jpeg', 5, 6, 2),
(70, 'Tablet Lenovo Yoga 10 Snapdragon 64gb 4ram ', 999000, '17', 'imagenes/20220328033905.jpeg', 5, 6, 2),
(71, 'Tablet Xiaomi Pad 5 - 128GB cosmic', 1708700, '17', 'imagenes/20220328034135.jpeg', 5, 4, 2),
(72, 'Xiaomi Mi 11 Lite 5G NE Dual SIM 256 GB ', 1379900, '14', 'imagenes/20220328034407.jpeg', 5, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `administrador_idadministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `nombre`, `apellido`, `correo`, `clave`, `estado`, `administrador_idadministrador`) VALUES
(5, 'David', 'Ortiz', 'do@123.com', 'd4579b2688d675235f402f6b4b43bcbf', 1, 1),
(7, 'Diana', 'Letrado', 'dl@123.com', '01a120c756b1f6cf4f08e0fca0cfa6fe', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoproducto`
--

CREATE TABLE `tipoproducto` (
  `idtipoproducto` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipoproducto`
--

INSERT INTO `tipoproducto` (`idtipoproducto`, `nombre`) VALUES
(1, 'Celular'),
(2, 'Tablet'),
(3, 'Juego'),
(4, 'Consola');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadministrador`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`idcarrito`,`cliente_idcliente`),
  ADD KEY `fk_carrito_cliente1_idx` (`cliente_idcliente`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `domiciliario`
--
ALTER TABLE `domiciliario`
  ADD PRIMARY KEY (`iddomiciliario`);

--
-- Indices de la tabla `envio`
--
ALTER TABLE `envio`
  ADD PRIMARY KEY (`idenvio`,`factura_idfactura`,`domiciliario_iddomiciliario`),
  ADD KEY `fk_envio_factura_idx` (`factura_idfactura`),
  ADD KEY `fk_envio_domiciliario_idx` (`domiciliario_iddomiciliario`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idfactura`,`cliente_idcliente`),
  ADD KEY `fk_factura_cliente1_idx` (`cliente_idcliente`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `pcarrito`
--
ALTER TABLE `pcarrito`
  ADD PRIMARY KEY (`carrito_idcarrito`,`producto_idproducto`),
  ADD KEY `fk_pcarrito_producto1_idx` (`producto_idproducto`);

--
-- Indices de la tabla `pfactura`
--
ALTER TABLE `pfactura`
  ADD PRIMARY KEY (`factura_idfactura`,`producto_idproducto`),
  ADD KEY `fk_facturaproducto_factura1_idx` (`factura_idfactura`),
  ADD KEY `fk_facturaproducto_producto1_idx` (`producto_idproducto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`,`proveedor_idproveedor`,`marca_idmarca`,`tipoproducto_idtipoproducto`),
  ADD KEY `fk_producto_marca_idx` (`marca_idmarca`),
  ADD KEY `fk_producto_tipoproducto1_idx` (`tipoproducto_idtipoproducto`),
  ADD KEY `fk_producto_proveedor1_idx` (`proveedor_idproveedor`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`,`administrador_idadministrador`),
  ADD UNIQUE KEY `correo_UNIQUE` (`correo`),
  ADD KEY `fk_proveedor_administrador1_idx` (`administrador_idadministrador`);

--
-- Indices de la tabla `tipoproducto`
--
ALTER TABLE `tipoproducto`
  ADD PRIMARY KEY (`idtipoproducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `idcarrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `domiciliario`
--
ALTER TABLE `domiciliario`
  MODIFY `iddomiciliario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `envio`
--
ALTER TABLE `envio`
  MODIFY `idenvio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idfactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipoproducto`
--
ALTER TABLE `tipoproducto`
  MODIFY `idtipoproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `fk_carrito_cliente1` FOREIGN KEY (`cliente_idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `envio`
--
ALTER TABLE `envio`
  ADD CONSTRAINT `fk_envio_domiciliario` FOREIGN KEY (`domiciliario_iddomiciliario`) REFERENCES `domiciliario` (`iddomiciliario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_envio_factura` FOREIGN KEY (`factura_idfactura`) REFERENCES `factura` (`idfactura`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_factura_cliente1` FOREIGN KEY (`cliente_idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pcarrito`
--
ALTER TABLE `pcarrito`
  ADD CONSTRAINT `fk_pcarrito_carrito1` FOREIGN KEY (`carrito_idcarrito`) REFERENCES `carrito` (`idcarrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pcarrito_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pfactura`
--
ALTER TABLE `pfactura`
  ADD CONSTRAINT `fk_facturaproducto_factura1` FOREIGN KEY (`factura_idfactura`) REFERENCES `factura` (`idfactura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_facturaproducto_producto1` FOREIGN KEY (`producto_idproducto`) REFERENCES `producto` (`idproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_marca` FOREIGN KEY (`marca_idmarca`) REFERENCES `marca` (`idmarca`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_producto_proveedor1` FOREIGN KEY (`proveedor_idproveedor`) REFERENCES `proveedor` (`idproveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_producto_tipoproducto1` FOREIGN KEY (`tipoproducto_idtipoproducto`) REFERENCES `tipoproducto` (`idtipoproducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `fk_proveedor_administrador1` FOREIGN KEY (`administrador_idadministrador`) REFERENCES `administrador` (`idadministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
