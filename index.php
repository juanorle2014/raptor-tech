
<?php
session_start();
require_once 'logica/Administrador.php';
require_once 'logica/Producto.php';
require_once 'logica/Marca.php';
require_once 'logica/TipoProducto.php';
require_once 'logica/Proveedor.php';
require_once 'logica/Cliente.php';
require_once 'logica/Carrito.php';
require_once 'logica/PCarrito.php';
require_once 'logica/Factura.php';
require_once 'logica/PFactura.php';
require_once 'logica/Domiciliario.php';
require_once 'logica/Envio.php';






if(isset($_GET["sesion"]) && $_GET["sesion"] == "false"){
    $_SESSION["id"] = "";
    $_SESSION["rol"] = "";
}

$pid = "";
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}

$paginasSinSesion = array(
    "presentacion/ingresar.php",
    "presentacion/autenticar.php",
    "presentacion/inicio.php",
    "presentacion/cliente/formularioRegistrar.php",
    "presentacion/domiciliario/registro.php"
)
?>

<html>
<head>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" ></script>
<script
	src="https://www.gstatic.com/charts/loader.js"></script>  
    <title>Raptor-Tech</title>
    <link rel="icon" type="image/jpeg" href="img/logo.jpeg"/>
</head>
<body background="img/fondo.gif">
<?php
if ($pid != "") {
    if(in_array($pid, $paginasSinSesion)){
        include $pid;
    }else{
        if(isset($_SESSION["id"]) && $_SESSION["id"] != ""){
            include $pid;
        }else{
            include 'presentacion/ingresar.php';
        }
    }
} else {
    include 'presentacion/inicio.php';
}
?>
</body>
</html>