<?php
echo $_SESSION["carrito"];
$carrito=new Carrito($_SESSION["carrito"],"","");
$productosCarrito = $carrito -> ConsultarProductos();
$total= $carrito -> ConsultarPrecio();
$p=count($productosCarrito);

for($i=0;$i<$p;$i++){
    
    if(isset($_POST["bot".$i])){
        
       $idp= $productosCarrito[$i] -> getid();
       $pcarrito= new PCarrito($_SESSION["carrito"],$idp,$_POST["cant".$i]);
       $pcarrito -> CambiarCantidad();
       $productosCarrito = $carrito -> ConsultarProductos();
       $total= $carrito -> ConsultarPrecio();
    }
     if(isset($_POST["elim".$i])){
        
        $idp= $productosCarrito[$i] -> getid();
        $pcarrito= new PCarrito($_SESSION["carrito"],$idp,"");
        $pcarrito -> BorrardelCarrito();
        $productosCarrito = $carrito -> ConsultarProductos();
        $total= $carrito -> ConsultarPrecio();
    }
}
include "presentacion/menuCliente.php";


?>

<div class="container">
	
	<br>
	<h3 align="left" >Productos en el Carrito</h3>

<?php if(($_SESSION['car'])!=0){?>

<table class="table">
		<tbody>
			<tr>
				<th width="40%" class="text-center">Descripcion</th>
				<th width="15%" class="text-center">Cantidad</th>
				<th width="20%" class="text-center">Precio</th>
				<th width="20%" class="text-center">Total</th>
				<th width="5%" class="text-center"><i class="far fa-trash-alt"></i></th>

			</tr>
<?php
        $pos = 0;

         foreach ($productosCarrito as $productoActual) {
             
             $id=$productoActual -> getId();
             $prod= new Producto($id);
             $prod ->Consultar();
             $cantidad=$prod ->getCantidad();
             
         $pos ++;
        ?>
  
			<tr>
				<td width="40%" class="text-center"><?php echo $productoActual -> getNombre()?></td>
				<td class="text-center">
					<form
						action="index.php?pid=<?php echo  base64_encode("carrito.php")?>"
						method="post">
						<div class=row>
						
							<?php if($cantidad>=5){?>
                    		<select class="form-select"
									name=<?php echo "'cant" .($pos-1). "'" ?>>
									
                              
                    			<option value="1" <?php if($productoActual -> getCantidad() == 1) echo "selected"?>>1</option>
                    			<option value="2" <?php if($productoActual -> getCantidad() == 2) echo "selected"?>>2</option>
                    			<option value="3" <?php if($productoActual -> getCantidad() == 3) echo "selected"?>>3</option>
                    			<option value="4" <?php if($productoActual -> getCantidad() == 4) echo "selected"?>>4</option>
                    			<option value="5" <?php if($productoActual -> getCantidad() == 5) echo "selected"?>>5</option>
                    
                    		</select>
                    		<?php } else if( $cantidad < 5){ ?>
                    		
                    		   <select class="form-select"
									name=<?php echo "'cant" .($pos-1). "'" ?>>
									
                    		<?php  for($i = 1 ; $i<=$cantidad;$i++) {  ?>
                    		    <option value="<?php echo $i ?>" <?php  if($productoActual -> getCantidad() == $i) echo "selected" ?>><?php echo $i?> </option>
                    		<?php    }?>
                    		    </select>
                         <?php   }    ?>
                    		    
					
								</div>
								<div class="row">
								<button class="btn btn-success" type="submit"
									name=<?php echo "'bot" .($pos-1). "'" ?>>Cambiar</button>
							    </div>
						
					</form>
				</td>

				<td width="20%" class="text-center"><p>$<?php echo number_format($productoActual -> getPrecio(),0)?> COP</p></td>
				<td width="20%" class="text-center"><p>$<?php echo number_format( ($productoActual -> getPrecio()) * ($productoActual -> getCantidad()) ,2) ?> COP</p></td>
				<td width="5%">
					<form
						action="index.php?pid=<?php echo  base64_encode("carrito.php")?>"
						method="post">
						<button class="btn btn-danger"
							name=<?php echo "'elim" .($pos-1). "'" ?> type="submit">Eliminar</button>

					</form>
				</td>

			</tr>

<?php } ?>
			<tr>
				<td colspan="4" align="left"><h3 style="color:white;">Total : $<?php echo number_format($total,2)?> COP</h3></td>
				<td align="center">
				<td>
			
			</tr>
			<tr>
				<td colspan="6" align="left"><form
						action="index.php?pid= <?php echo base64_encode("comprar.php")?>"
						method="post">
						<input type="hidden" name="valor" value="<?php echo $total?>">
						<button class="btn btn-success" name="buy" type="submit"><i class="fas fa-comment-dollar"></i> Comprar</button>

					</form></td>
			</tr>

		</tbody>
	</table>
	
<?php } else { ?>

<div class="alert alert-success">No hay productos en su carrito</div>

<?php }?>
</div>


