<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ProveedorDAO.php';
class Proveedor{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $admin;
    private $estado;
    private $conexion;
    private $proveedorDAO;
    
    
    public function getId()
    {
        return $this->id;
    }

    
    public function getNombre()
    {
        return $this->nombre;
    }

   
    public function getApellido()
    {
        return $this->apellido;
    }

  
    public function getCorreo()
    {
        return $this->correo;
    }

    
    public function getClave()
    {
        return $this->clave;
    }

   
    public function getAdmin()
    {
        return $this->admin;
    }

    
    public function getEstado()
    {
        return $this->estado;
    }

    public function Proveedor($id="", $nombre="", $apellido="", $correo="", $clave="", $admin="", $estado=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> admin = $admin;
        $this -> estado= $estado;
        $this -> conexion = new Conexion();
        $this -> proveedorDAO = new ProveedorDAO($id, $nombre, $apellido, $correo, $clave, $admin, $estado);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        //echo $this -> proveedorDAO -> autenticar();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            
            $datos = $this -> conexion -> extraer();
            $this -> id =$datos[0];
            $this -> estado=$datos[1];
            
            
            return true;
        }else{
            return false;
        }                
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        //echo $this -> proveedorDAO -> consultar();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        //echo $this -> nombre = $datos[0];
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];        
    }

    public function Crear(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> Crear());
        $this -> conexion -> cerrar();
    }

    public function Activar(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> Activar());
        $this -> conexion -> cerrar();
    }
    
    public function Deshabilitar(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> Deshabilitar());
        $this -> conexion -> cerrar();
    }
    
    public function ConsultarEstado(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> ConsultarEstado());
        $resultado = $this -> conexion -> extraer();
        $this -> estado = $resultado[0];
    }

    public function ConsultarporProv($pag, $regPag){
            
        $this -> conexion -> Abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> ConsultarporProv($pag, $regPag));
        $productos = array();
        
        while(($resultado = $this -> conexion -> extraer()) != null){
            
            $proveedor = new Proveedor($resultado[4]);
            $proveedor -> Consultar();
            
            $marca = new Marca($resultado[5]);
            $marca -> Consultar();
            
            $tipoproducto = new TipoProducto($resultado[6]);
            $tipoproducto -> Consultar();
            
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $proveedor, $marca, $tipoproducto, $resultado[7]));
        }
        
        $this -> conexion -> cerrar();
        
        return $productos;
    }

    public function ConsultarTotalFilasporProv(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarNumRegProv());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function ConsultarVendidosvsinv(){
            
        $this -> conexion -> Abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> ConsultarVendidosvsinv());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($productos, $resultado);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function ConsultarVendidos(){
        
        $this -> conexion -> Abrir();
        $this -> conexion -> ejecutar($this -> proveedorDAO -> ConsultarVendidos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($productos, $resultado);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
}

?>