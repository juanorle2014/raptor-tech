<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/EnvioDAO.php';

class Envio{
    private $id;
    private $factura;
    private $domiciliario;
    private $cliente;
    private $conexion;
    private $EnvioDAO;
    
    
    public function getId()
    {
        return $this->id;
    }

    public function getFactura()
    {
        return $this->factura;
    }
    
    public function getDomiciliario()
    {
        return $this->domiciliario;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function Envio($id="", $factura="", $domiciliario="", $cliente=""){
        $this -> id = $id;
        $this -> factura = $factura;
        $this -> domiciliario = $domiciliario;
        $this -> cliente = $cliente;
        $this -> conexion = new Conexion();
        $this -> envioDAO = new EnvioDAO($id, $factura, $domiciliario,$cliente);
    }
    
    public function Crear(){
        
        $this -> conexion -> abrir();
       // echo $this -> envioDAO ->Crear();
        $this -> conexion -> ejecutar($this -> envioDAO ->Crear());
        $this -> conexion -> ejecutar($this -> envioDAO -> ConsultarUltimoId());
        $resultado= $this -> conexion ->extraer();
        $this -> conexion -> cerrar();
        
        return $resultado[0];
        
    }

    public function Buscar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> envioDAO -> Buscar());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){

            $factura=new Factura($resultado[1]);
            $res=$factura->ConsultarDom();


            $dom=new Domiciliario($resultado[2]);
            $dom -> Consultar();

            array_push($productos, new Envio($resultado[0], $resultado[1],$dom,$res));
        }
        $this -> conexion -> cerrar();
        return $productos;
    }


    
   
    
}

?>