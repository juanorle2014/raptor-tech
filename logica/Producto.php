<?php 
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";

class Producto{
    
    private $id;
    private $nombre;
    private $precio;
    private $cantidad;
    private $marca;
    private $tipoproducto;
    private $proveedor;
    private $imagen;
    private $conexion;
    private $prouctoDAO;
    /**
     * @return mixed
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * @param mixed $proveedor
     */
   
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @return mixed
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @return mixed
     */
    public function getTipoProducto()
    {
        return $this->tipoproducto;
        
    }

    public function getImagen(){
        return $this->imagen;
        
    }
    
   

    
   
    
    public function Producto($id="", $nombre="", $precio="", $cantidad="", $proveedor="", $marca="", $tipoProducto="", $imagen=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> precio = $precio;
        $this -> cantidad = $cantidad;
        $this -> proveedor = $proveedor;
        $this -> marca = $marca;
        $this -> tipoproducto = $tipoProducto;
        $this -> imagen = $imagen;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($id, $nombre, $precio, $cantidad, $proveedor, $marca, $tipoProducto, $imagen);
    }
    
    public function crear(){
        $this -> conexion -> abrir();
        echo $this -> productoDAO -> crear();
        $this -> conexion -> ejecutar($this -> productoDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        //echo $this -> productoDAO -> consultarTodos();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $proveedor = new Proveedor($resultado[4]);
          // echo $resultado[4];
            $proveedor -> consultar();
            $marca = new Marca($resultado[5]);
            //echo $resultado[5];
            $marca -> consultar();
            
            $tipoproducto = new TipoProducto($resultado[6]);
            //echo $resultado[5];
            $tipoproducto -> consultar();
            
            
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $proveedor, $marca, $tipoproducto,$resultado[7]));
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    
    
    public function consultarTodosPag($pag, $regPag){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodosPag($pag, $regPag));
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $proveedor = new Proveedor($resultado[4]);
            $proveedor -> consultar();
            $marca = new Marca($resultado[5]);
            $marca -> consultar();
            $tipoproducto = new TipoProducto($resultado[6]);
            $tipoproducto -> consultar();
            array_push($productos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $proveedor, $marca, $tipoproducto, $resultado[7]));
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
    public function consultarNumReg(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarNumReg());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
    public function Consultar(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> Consultar());
        $resultado = $this -> conexion -> extraer();
        
        $proveedor = new Proveedor($resultado[4]);
        $proveedor -> consultar();
        $marca = new Marca($resultado[5]);
        $marca -> consultar();
        $tipoproducto = new TipoProducto($resultado[6]);
        $tipoproducto -> consultar();
        $this -> id = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> cantidad = $resultado[3];
        $this -> imagen = $resultado[7];
        $this -> proveedor = $proveedor;
        $this -> marca = $marca;
        $this -> tipoproducto = $tipoproducto;
    }
    
    public function Agregar() {
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> Agregar());
        $this -> conexion -> cerrar();
        
    }
    
    public function ModPrecio() {
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> ModPrecio());
        $this -> conexion -> cerrar();
        
    }
    
    public function EditarImagen(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> EditarImagen());
        $this -> conexion -> cerrar();
    }

    public function consultarProductosPorMarca(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductosPorMarca());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
        
        public function consultarProductosPorTipo(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductosPorTipo());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
        public function consultarProductosPorPrecio(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductosPorPrecio());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
        public function consultarProductosPorProveedor(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductosPorProveedor());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
        
        public function ConsultarVendidosvsinv(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> ConsultarVendidosvsinv());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
        
        public function ConsultarVendidos(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> ConsultarVendidos());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }

        public function consultarEnvio(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarEnvio());
            $productos = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                array_push($productos, $resultado);
            }
            $this -> conexion -> cerrar();
            return $productos;
        }
    
}



?>