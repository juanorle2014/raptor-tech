<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/DomiciliarioDAO.php';
class Domiciliario{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $estado;
    private $conexion;
    private $DomiciliarioDAO;
    
    
    public function getId()
    {
        return $this->id;
    }

    
    public function getNombre()
    {
        return $this->nombre;
    }

   
    public function getApellido()
    {
        return $this->apellido;
    }

  
    public function getCorreo()
    {
        return $this->correo;
    }

    
    public function getClave()
    {
        return $this->clave;
    }
    
    public function getEstado()
    {
        return $this->estado;
    }

    public function Domiciliario($id="", $nombre="", $apellido="", $correo="", $clave="", $estado=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> estado= $estado;
        $this -> conexion = new Conexion();
        $this -> domiciliarioDAO = new DomiciliarioDAO($id, $nombre, $apellido, $correo, $clave, $estado);
    }
    
    public function Crear(){
        
        $this -> conexion -> abrir();
        echo $this -> domiciliarioDAO -> Crear();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> Crear());
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> ConsultarUltimoId());
        $resultado = $this -> conexion -> extraer();
        $this -> conexion -> cerrar();
        
        return $resultado[0];
    }
    
    public function Autenticar(){
        
        $this -> conexion -> abrir();
       // echo $this -> domiciliarioDAO -> Autenticar();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> Autenticar());
        
        if($this -> conexion -> numFilas() == 0){
            
            return false;
        }else{
            
            $resultado = $this -> conexion -> extraer();
            $this -> id = $resultado[0];
            $this -> estado = $resultado[1];
            
            return true;
        }
    }

    public function Activar(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> Activar());
        $this -> conexion -> cerrar();
    }
    
    public function Deshabilitar(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> Deshabilitar());
        $this -> conexion -> cerrar();
    }

    public function ConsultarEstado(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> ConsultarEstado());
        $resultado = $this -> conexion -> extraer();
        $this -> estado = $resultado[0];
    }

    public function Consultar() {
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> Consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        ;
    }

    public function BuscarDom(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> domiciliarioDAO -> BuscarDom());
        $facturas= array();
        
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($facturas, new Factura($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
        }
        
        $this -> conexion -> cerrar();
        
        return $facturas;
     }

    
    
}

?>