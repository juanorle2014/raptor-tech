<?php
require_once 'persistencia/FacturaDAO.php';
require_once 'persistencia/Conexion.php';

class Factura
{
    private $id;
    private $total;
    private $idcliente;
    private $estado;
    private $conexion;
    private $facturaDAO;


    public function getId()
    {
        return $this->id;
    }
    public function getTotal()
    {
        return $this->total;
    }
    public function getIdcliente()
    {
        return $this->idcliente;
    }
    public function getEstado()
    {
        return $this->estado;
    }
    
    public function Factura($id="",$total="",$idc="", $estado=""){
        
        $this -> id = $id;
        $this -> total = $total;
        $this -> idcliente = $idc;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> facturaDAO = new FacturaDAO($id, $total, $idc, $estado);
    }
    
    public function CrearFactura(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO ->CrearFactura());
        $this -> conexion -> ejecutar($this -> facturaDAO -> ConsultarUltimoId());
        $resultado= $this -> conexion ->extraer();
        $this -> conexion -> cerrar();
        
        return $resultado[0];
        
    }
    
    public function ConsultarProductos(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> ConsultarProductos());
        $productos = array();
        
        while(($resultado = $this -> conexion -> extraer()) != null){
            
            $productoaux=new Producto($resultado[0]);
            $productoaux -> Consultar();
            
            array_push($productos,new Producto($resultado[0],$productoaux->getNombre(),$productoaux->getPrecio(),$resultado[1],"","","",""));
            
        }
        
        $this -> conexion -> cerrar();
        
        return $productos;
    }
    
    public function Consultar(){
       
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO ->Consultar());
        $resultado= $this -> conexion ->extraer();
        $this -> conexion -> cerrar();
        
        return $resultado;
    }

    public function ANoDisponible(){
            
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> ANoDisponible());
        $this -> conexion -> cerrar();
    }
    
    public function ADisponible(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> ADisponible());
        $this -> conexion -> cerrar();
    }

    public function ConsultarEstado(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO -> ConsultarEstado());
        $resultado = $this -> conexion -> extraer();
        $this -> estado = $resultado[0];
    }

    public function ConsultarDom(){
       
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> facturaDAO ->ConsultarDom());
        $resultado= $this -> conexion ->extraer();
        
        $this -> conexion -> cerrar();
        
        return $resultado;
    }
  
}

