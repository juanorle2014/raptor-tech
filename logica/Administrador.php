<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/AdministradorDAO.php';
class Administrador{
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $conexion;
    private $administradorDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getApellido()
    {
        return $this->apellido;
    }

    public function getCorreo()
    {
        return $this->correo;
    }

    public function Administrador($id="", $nombre="", $apellido="", $correo="", $clave=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> conexion = new Conexion();
        $this -> administradorDAO = new AdministradorDAO($this -> id, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $this -> id = $this -> conexion -> extraer()[0];
            return true;
        }else{
            return false;
        }                
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        $this -> nombre = $datos[0];
        $this -> apellido = $datos[1];
        $this -> correo = $datos[2];        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarTodos());
        $administradores = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($registro[0], $registro[1], $registro[2], $registro[3], "");
            array_push($administradores, $administrador);
        }
        $this -> conexion -> cerrar();
        return  $administradores;
    }

    public function ConsultarProveedores(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> ConsultarProveedores());
        $proveedores = array();
        
        while(($resultado = $this -> conexion -> extraer()) != null){
            $admin= new Administrador($resultado[5]);
            $admin -> consultar();
            array_push($proveedores, new Proveedor($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $admin, $resultado[4]));
        }
        
        $this -> conexion -> cerrar();
        
        return $proveedores;
     }
     
     public function ConsultarClientes(){
         
         $this -> conexion -> abrir();
         $this -> conexion -> ejecutar($this -> administradorDAO -> ConsultarClientes());
         $clientes = array();
         
         while(($resultado = $this -> conexion -> extraer()) != null){
             array_push($clientes, new Cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4], $resultado[5]));
         }
         
         $this -> conexion -> cerrar();
         
         return $clientes;
     }

     public function ConsultarDom(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> ConsultarDom());
        $proveedores = array();
        
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($proveedores, new Domiciliario($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4]));
        }
        
        $this -> conexion -> cerrar();
        
        return $proveedores;
     }
    
    
    
}

?>